﻿using System;
using System.IO;
using CourseProject;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseProjectTest
{
    [TestClass]
    public class WorksWithFileTests
    {
        public TestContext TestContext { get; set; }

        /*
         * Тестирование метода Save из класса WorksWithFiles
         * Данные для тестирования берутся из файла DataTestFiles.xml (строки начинаются с Save)
         */
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "DataTestFiles.xml",
            "Save",
            DataAccessMethod.Sequential)]
        [TestMethod]
        public void WorksWithFiles_SaveTest_FileExists()
        {
            //arrange
            string fileLocation = Convert.ToString(TestContext.DataRow["fileLocation"]);
            string text = Convert.ToString(TestContext.DataRow["text"]);
            bool expected = Convert.ToBoolean(TestContext.DataRow["expected"]);

            //act
            bool actual = WorksWithFiles.Save(fileLocation, text);

            //assert
            Assert.AreEqual(expected, actual);
            if (expected)
            {
                Assert.AreEqual(text, File.ReadAllText(fileLocation, System.Text.Encoding.Default));
            }
        }

        /*
         * Тестирование метода Open из класса WorksWithFiles
         * Данные для тестирования берутся из файла DataTestFiles.xml (строки начинаются с Open)
         */
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "DataTestFiles.xml",
            "Open",
            DataAccessMethod.Sequential)]
        [TestMethod]
        public void WorksWithFiles_OpenTest_Text()
        {
            //arrange
            string fileLocation = Convert.ToString(TestContext.DataRow["fileLocation"]);
            string expected = Convert.ToString(TestContext.DataRow["expected"]);
            if(fileLocation == "InputText.txt")
            {
                WorksWithFiles.Save(fileLocation, expected);
            }
            if(expected == "null")
            {
                expected = null;
            }

            //act
            string actual = WorksWithFiles.Open(fileLocation);

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
