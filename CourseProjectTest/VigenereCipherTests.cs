﻿using System;
using System.IO;
using CourseProject;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseProjectTests
{
    [TestClass]
    public class VigenereCipherTests
    {
        public TestContext TestContext { get; set; }

        /*
         * Тестирование метода Encryption из класса VigenereCipher
         * Данные для тестирования берутся из файла DataTestCipher.xml (строки начинаются с Encryption)
         */
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "DataTestCipher.xml",
            "Encryption",
            DataAccessMethod.Sequential)]
        [TestMethod]
        public void VigenereCipher_EncryptionTest_FromXML()
        {
            //arrange
            string text = Convert.ToString(TestContext.DataRow["InputText"]);
            string key = Convert.ToString(TestContext.DataRow["Key"]);
            string expected = Convert.ToString(TestContext.DataRow["OutputText"]);

            //act
            string actual = VigenereCipher.Encryption(key, text);

            //assert
            Assert.AreEqual(expected, actual);
        }

        /*
        * Тестирование метода Decryption из класса VigenereCipher
        * Данные для тестирования берутся из файла DataTestCipher.xml (строки начинаются с Decryption)
        */
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "DataTestCipher.xml",
            "Decryption",
            DataAccessMethod.Sequential)]
        [TestMethod]
        public void VigenereCipher_DecryptionTest_FromXML()
        {
            //arrange
            string text = Convert.ToString(TestContext.DataRow["InputText"]);
            string key = Convert.ToString(TestContext.DataRow["Key"]);
            string expected = Convert.ToString(TestContext.DataRow["OutputText"]);

            //act
            string actual = VigenereCipher.Decryption(key, text);

            //assert
            Assert.AreEqual(expected, actual);
        }

        /*
        * Тестирование метода CheckKey из класса VigenereCipher
        * Данные для тестирования берутся из файла DataTestCipher.xml (строки начинаются с Key)
        */
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "DataTestCipher.xml",
            "Key",
            DataAccessMethod.Sequential)]
        [TestMethod]
        public void VigenereCipher_KeyTest_FromXML()
        {
            //arrange
            string key = Convert.ToString(TestContext.DataRow["key"]);
            bool expected = Convert.ToBoolean(TestContext.DataRow["expected"]);

            //act
            bool actual = VigenereCipher.CheckKey(key);

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
