﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject
{
    public static class VigenereCipher
    {
        // Алфавит, который используется для шифрования и дешифрования
        private static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        /*
         * Метод который проверяет правильность ключа для шифрования/дешифрования
         * Ключ должен содержать только симфолы из алфавита
         * Метод принимает ключ в виде строки
         * Возращает true, если ключ подходит для шифрования
         * Возращает false, если ключ не подходит для шифрования
         */
        public static bool CheckKey(string key)
        {
            // Проверяем, что ключ - это не пустая строка и не null
            if(string.IsNullOrEmpty(key))
            {
                return false;
            }

            // Переводим ключ в нижний регистр
            key = key.ToLower();

            // Проверяем, что ключ содержит только символы из алфавита
            foreach(var i in key)
            {
                if(alphabet.IndexOf(i) == -1)
                {
                    return false;
                }
            }
            return true;
        }

        /*
         * Метод предназначен для шифрования/дешифрования текста по методу Виженера
         * Возращает зашифрованный/дешифрованный текст по методу Виженера в виде строки
         * Принимает ключ в виде строки, текст для шифрования, и encrypt.
         * Encrypt = true - зашифровать, false - дешифровать.
         * Возращает null, если ключ не подходит или текст пустой или null
         */
        private static string Vigenere(string key, string text, bool encrypt = true)
        {
            // Проверяем правильность ключа и текста
            if(!CheckKey(key) || string.IsNullOrEmpty(text))
            {
                return null;
            }

            //Строка для хранения повторяющего ключа
            string RapeaterKey = "";
            // Строка для хранения возращаемого текста
            string returnText = "";
            // Переводим ключ в нижний регистр
            key = key.ToLower();
            // Счетчикб чтобы переберать символы в повторяющем ключе
            int j = -1;

            // Повторяем исходный ключ до тех пор пока он не будет равен длине текста
            while (RapeaterKey.Length < text.Length)
            {
                RapeaterKey += key;
            }
            RapeaterKey = RapeaterKey.Substring(0, text.Length);

            // Перебераем все символы в тексте
            for (int i = 0; i < text.Length; i++)
            {
                // Если символ это буква
                if (char.IsLetter(text[i]))
                {
                    char temp = text[i];
                    // Индекс в алфавите символа из текста
                    int IndexOfLetter = alphabet.IndexOf(Char.ToLower(temp));
                    // Если данный символ не присутствует в алфавите, то добавляем без изменений 
                    if (IndexOfLetter == -1)
                    {
                        returnText += text[i].ToString();
                    }
                    // Если символ из алфавита
                    else
                    {
                        // Переменная для хранения возращаемого индекса
                        int retunIndex;
                        j++;
                        // Индекс для хранения индекса в алфавите из ключа
                        int IndexOfKey = alphabet.IndexOf(RapeaterKey[j]);
                        // Если текст шифруется
                        if (encrypt)
                        {
                            retunIndex = IndexOfLetter + IndexOfKey;
                            if (retunIndex >= alphabet.Length)
                            {
                                retunIndex -= alphabet.Length;
                            }
                        }
                        // Если дешифруется
                        else
                        {
                            retunIndex = IndexOfLetter - IndexOfKey;
                            if (retunIndex < 0)
                            {
                                retunIndex += alphabet.Length;
                            }
                        }
                        // Если исходная буква в верхнем регистре
                        if (Char.IsUpper(text[i]))
                        {
                            returnText += alphabet[retunIndex].ToString().ToUpper();
                        }
                        else
                        {
                            returnText += alphabet[retunIndex].ToString();
                        }
                    }
                }
                // Если не буква, то добавляем тексту без изменений 
                else
                {
                    returnText += text[i];
                }
            }
            return returnText;
        }

        // Метод, который шифрует текст по методу Виженера
        public static string Encryption(string key, string text) => Vigenere(key, text);

        // Метод, который дешифрует текст по методу Виженера
        public static string Decryption(string key, string text) => Vigenere(key, text, false);
    }
}
