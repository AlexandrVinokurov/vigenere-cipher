﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseProject
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string Input;
        public static string Output;
        public MainWindow()
        {
            InitializeComponent();
        }

        /* 
         * Если поставлена галочка ключ слово: скорпион
         * 1) Очистить поле для ввода ключа
         * 2) Заблокировать ввод ключа через поле
         */
        private void CheckBoxKeyScorpio_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxKeyWord.Clear();
            TextBoxKeyWord.IsReadOnly = true;
        }

        /*
         * Если галочка снята:
         * Разблокировать поле для ввода ключа
         */
        private void CheckBoxKeyScorpio_Unchecked(object sender, RoutedEventArgs e)
        {
            TextBoxKeyWord.IsReadOnly = false;
        }

        // Нажатие на кнопку из меню "Открыть как"
        private void MenuItem_OpenAs_Click(object sender, RoutedEventArgs e)
        {
            // Очистить поля
            TextBoxInputText.Clear();
            TextBoxOutputText.Clear();

            // Вывести информацию из файла
            TextBoxInputText.Text = WorksWithFiles.OpenAs();
        }

        // Нажатие на кнопку из меню "Сохранить как"
        private void MenuItem_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            // Вызвать диалоговое окно для сохранения
            WorksWithFiles.SaveAs(TextBoxOutputText.Text);
        }

        // Нажатие на кнопку из меню "Открыть"
        private void MenuItem_Open_Click(object sender, RoutedEventArgs e)
        {
            // Открыть новое окно, где пользователь вводит путь до файла, который необходимо открыть
            new Open().ShowDialog();

            // Очистить поле для входного текста и вывести новый текст
            TextBoxInputText.Clear();
            TextBoxInputText.Text = Input;
        }

        // Нажатие на кнопку из меню "Сохранить"
        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            // Открыть новое окно, где пользователь вводит путь до файла, который необходимо сохранить
            Output = TextBoxOutputText.Text;
            new Save().ShowDialog();
        }

        // Нажатие на кнопку "Преобразовать"
        private void ButtonStartConvert_Click(object sender, RoutedEventArgs e)
        {
            // Строка для хранения ключа
            string key = null;
            // Очищаем поле для вывода текста
            TextBoxOutputText.Clear();

            // Проверяем, что выбрано склово скорпион
            if (CheckBoxKeyScorpio.IsChecked == true)
            {
                key = "скорпион";
            }
            else
            {
                /* 
                 * Проверяем ключ слово. 
                 * Если ключ введен неверно, то выводим сообщение: Ключ введен неверно!
                 */
                if (!VigenereCipher.CheckKey(TextBoxKeyWord.Text))
                {
                    MessageBox.Show("Ключ введен неверно!");
                } else
                {
                    key = TextBoxKeyWord.Text;
                }
            }
            // Проверяем, что ключ задан
            if(key != null)
            {
                // Строка для хранения текста, который необходимо преобразовать
                string text = TextBoxInputText.Text;
                // Если выбран режим шифрования
                if (RadioButtonEncrypt.IsChecked == true)
                {
                    TextBoxOutputText.Text = VigenereCipher.Encryption(key, text);
                }
                // Если выбран режим дешифрования
                else
                {
                    TextBoxOutputText.Text = VigenereCipher.Decryption(key, text);
                }
            }
        }

        // Нажатие на кнопку "Очистить текст"
        private void ClearInputText_Click(object sender, RoutedEventArgs e)
        {
            TextBoxInputText.Clear();
        }

        // Нажатие на кнопку "Очистить текст"
        private void ClearOutputText_Click(object sender, RoutedEventArgs e)
        {
            TextBoxOutputText.Clear();
        }
    }
}
