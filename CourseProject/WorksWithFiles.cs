﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject
{
    public static class WorksWithFiles
    {
        /*
         * Метод сохраняющий текст в файле формате .txt
         * Вызывает диалоговое окно, в котором пользователь выбирает путь для сохранения файла
         * Принимает текст в виде строки
         * Возращает true, если файл сохранен
         * Возращает false, если файл не сохранен
         */
        public static bool SaveAs(string text)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document"; // Название файла по умолчанию
            dlg.DefaultExt = ".text"; // Расширение файла по умолчанию 
            dlg.Filter = "Text documents (.txt)|*.txt"; // Фильт: только текстовые файлы

            // Показывает диалоговое для сохранения файла
            Nullable<bool> result = dlg.ShowDialog();

            // Если путь к фалйу указан правильно
            if (result == true)
            {
                // Сохранение файла
                if(Save(dlg.FileName, text))
                {
                    return true;
                }
            }
            return false;
        }

        /*
         * Метод сохраняющий файл по заданному пути
         * Принимает путь к фалу в виде строки, и текст для сохранения
         * Возращает false, если путь к файлу указан неверно
         * Возращает true, если файл сохранен
         */
        public static bool Save(string fileLocation, string text)
        {
            // Проверяем путь к фалу: строка не пустая или не null, и оканчивается на .txt
            if (string.IsNullOrEmpty(fileLocation) || !fileLocation.EndsWith(".txt"))
            {
                return false;
            }

            // Если путь файлу указан верно, то сохраняем файла в указанной директории
            File.WriteAllText(fileLocation, text, System.Text.Encoding.Default);
            return true;
        }

        /*
         * Метод открывающий файл
         * Вызывает диалоговое окно, в котором пользователь выбирает файл
         * Возращает null, если не получилось получить текст из файла или файн не найден
         * Возращает текст из файла
         */
        public static string OpenAs()
        {
            Microsoft.Win32.OpenFileDialog openFile = new Microsoft.Win32.OpenFileDialog();
            openFile.Filter = "Text documents (.txt)|*.txt"; // Фильт: только текстовые файлы
            openFile.DefaultExt = ".text"; // Расширение файла по умолчанию 

            // Показывает диалоговое для открытия файла
            Nullable<bool> result = openFile.ShowDialog();

            // Если путь к фалу указан верно
            if (result == true)
            {
                return Open(openFile.FileName);
            }
            return null;
        }

        /*
         * Метод открывающий файл
         * Принимает путь к фалу в виде строки
         * Возращает null, если путь к файлу указан неверно
         * Возращает текст из файла
         */
        public static string Open(string fileLocation)
        {
            // Проверяем путь к файлу: строка не пустая или null, файл существует и окначивается на .txt
            if (string.IsNullOrEmpty(fileLocation) || !File.Exists(fileLocation) || !fileLocation.EndsWith(".txt"))
            {
                return null;
            }

            // Считываем текст из файла
            return File.ReadAllText(fileLocation, System.Text.Encoding.Default);
        }
    }
}
